# Java HTTP Client

Een eenvoudige HTTP client in Java. Maakt verbinding met de [Fake Weather API](https://gitlab.com/student155/web-api/-/blob/master/README.md) via HTTP en verwerkt de opgehaalde JSON-data.

## Verdere opties

1. Stel we willen twee typen weer-data opslaan: van zowel steden als landen, verzin hier een klassenstructuur voor. Kun je hierbij gebruik maken van inheritance? En van een abstract class of interface? (voor voorbeeld, zie branch [inheritance](https://gitlab.com/adsd-hu/java-http-client/-/tree/inheritance)).

2. De WeatherService class doet nu twee dingen: 1) een HTTP-request afhandelen, en 2) JSON parsen. Kun je deze functionaliteit opsplitsen? (voor voorbeeld, zie branch [adds-parser-class](https://gitlab.com/adsd-hu/java-http-client/-/tree/adds-parser-class)).

3. De WeatherService class is nu specifiek voor het ophalen van weer-data. Kunnen we deze class generieker maken, bv. APIService?

## Meer informatie
- [Baeldung: Do a Simple HTTP Request in Java](https://www.baeldung.com/java-http-request)
- [JSON.simple – Read and write JSON](https://mkyong.com/java/json-simple-example-read-and-write-json/)