package io.cp3;

public class Main {

    public static void main(String[] args) {

        // create a service object to fetch weather for city of Amersfoort
        WeatherService service = new WeatherService("Amersfoort");

        try {
            // fetch the data
            WeatherData weatherData = service.fetchWeatherData();

            // print out data to test result
            System.out.println(weatherData.getCity());
            System.out.println(weatherData.getTemperature());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
