package io.cp3;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class WeatherService {
    
    private String urlString = "https://adsd-weather-api.herokuapp.com/weather/";
    private String queryString;

    // constructor with city name
    public WeatherService(String city) {
        this.queryString = "city=" + city;
    }

    public WeatherData fetchWeatherData() throws Exception {

        // create URL and fetch data from HTTP connection
        URL url = new URL(urlString + "?" + queryString);
        HttpURLConnection connection = (HttpURLConnection)url.openConnection();
        InputStreamReader inputStreamReader = new InputStreamReader(connection.getInputStream());

        // parse JSON input stream into WeatherData object
        WeatherData weatherData = this.parseInputStream(inputStreamReader);

        return weatherData;
    }

    private WeatherData parseInputStream(InputStreamReader input) throws Exception {
        // parse JSON from input stream to WeatherData object
        JSONParser parser = new JSONParser();
        JSONObject json = (JSONObject)parser.parse(input);

        // throw exceptions if fields are invalid missing
        String city = (String)json.get("city");
        if (city == null) {
            throw new Exception("City field missing or invalid value.");
        }
        Long temperature = (Long)json.get("temperature");
        if (temperature == null) {
            throw new Exception("Temperature field missing or invalid value.");
        }

        // if all fields are present, create and return WeatherData object
        return new WeatherData(city, temperature);
    }
}
