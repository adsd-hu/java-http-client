package io.cp3;

// data model object for weather data
public class WeatherData {

    private String city;
    private Long temperature;

    public WeatherData(String city, Long temperature) {
        this.city = city;
        this.temperature = temperature;
    }

    public String getCity() {
        return this.city;
    }

    public Long getTemperature() {
        return this.temperature;
    }
}
